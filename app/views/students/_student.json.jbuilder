json.extract! student, :id, :name, :last_name, :age, :average, :created_at, :updated_at
json.url student_url(student, format: :json)
